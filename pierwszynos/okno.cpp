#include "okno.h"
#include <iostream>



Trojkat::Trojkat()
{
	this->screenWidth = 1024;
	this->screenHeight = 768;
	this->posX = 100;
	this->posY = 100;
}

Trojkat::Trojkat(int screenWidth, int screenHeigth, int posX, int posY)
{
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeigth;
	this->posX = posX;
	this->posY = posY;
}




void Trojkat::inicjalizacjaGlew()

{
	GLenum wynik = glewInit();
	if (wynik != GLEW_OK)
	{
		std::cerr << "NIe udalo sie zainicjalizowac GlEW.Blad: " << glewGetErrorString(wynik) << std::endl;
		system("pause");
		exit(1);
	}
}



void Trojkat::CreateWindowGlut(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(screenWidth, screenHeight);
	glutInitWindowPosition(posX, posY);
	glutCreateWindow("Trojkat");
}

void Trojkat::Display()
{
	glClear(GL_COLOR_BUFFER_BIT);


	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, 3);


	glDisableVertexAttribArray(0);
	glutSwapBuffers();
}

void Trojkat::stworzenieVAO()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
}
void Trojkat::stworzenieVBO()
{
	GLfloat Wierzcholki[9] = { -1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f };

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Wierzcholki), Wierzcholki, GL_STATIC_DRAW);
}





int main(int argc, char ** argv)
{
	Trojkat Trojkat(786, 1024, 100, 100);


	glutDisplayFunc(Trojkat.Display);
	// po zamknieciu okna kontrola wraca do programu
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	Trojkat.CreateWindowGlut(argc, argv);
	Trojkat.inicjalizacjaGlew();



	Trojkat.stworzenieVAO();
	Trojkat.stworzenieVBO();
	Trojkat.Display();


	glClearColor(0.2f, 0.1f, 0.0f, 0.0f);

	glutMainLoop();

	return 0;
}




